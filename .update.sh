#!/bin/sh
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
LANG=C
MESSAGE="$(date -u '+%Y%m%d%H%M%S')"
git add .
git commit -m "$MESSAGE"
git push origin master
printf "\n...done\n"
exit 0
